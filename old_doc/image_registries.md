## Working with thirdparty container image registries

While it is recommended to work with the CSCS internal provided registry due to fastest network connection, it is also possible to work with thirdparty registries like dockerhub.com or quay.io.
When you work with thirdparty registries, then you have to provide login credentials for the CI jobs.

There are two possible ways to push images to thirdparty registries (it is assumed that you have stored the variable `DOCKERHUB_ACCESS_TOKEN` at the CI setup page):

The first approach will push both to the CSCS registry and additionally to the thirdparty registry at dockerhub.com
```yaml
my_job:
  extends: .container-builder-cscs-zen2
  stage: my_stage
  variables:
    DOCKERFILE: path/to/Dockerfile
    PERSIST_IMAGE_NAME: $CSCS_REGISTRY_PATH/my_image:${CI_COMMIT_SHORT_SHA}
    SECONDARY_IMAGE_NAME: docker.io/your_dockerhub_username/my_image:${CI_COMMIT_SHORT_SHA}
    SECONDARY_IMAGE_USERNAME: your_dockerhub_username
    SECONDARY_IMAGE_PASSWORD: $DOCKERHUB_ACCESS_TOKEN
```

The second approach will only push to a thirdparty registry at dockerhub.com:
```yaml
my_job:
  extends: .container-builder-cscs-zen2
  stage: my_stage
  variables:
    DOCKERFILE: path/to/Dockerfile
    PERSIST_IMAGE_NAME: docker.io/your_dockerhub_username/my_image:${CI_COMMIT_SHORT_SHA}
    CUSTOM_REGISTRY_USERNAME: your_dockerhub_username
    CUSTOM_REGISTRY_PASSWORD: $DOCKERHUB_ACCESS_TOKEN
```