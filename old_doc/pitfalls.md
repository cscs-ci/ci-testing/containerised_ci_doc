## Pitfalls
While we leverage gitlab itself to run pipelines, there are known differences to the [official documentation](https://docs.gitlab.com/ee/ci/yaml/).

Here you can find a list of things that for sure will not work as described in the documentation:
- `CI_PIPELINE_SOURCE` is always `trigger`, and never any other value. I.e. it is impossible to figure out by inspecting this variable whether this is a pipeline on a PR/MR, or a push event.
- `rules: changes` will not work for the above reason, you are never running a **branch pipeline** or **merge request pipeline**
- `only / except` and `rules` can restrict for which branches a job should run, but you should keep in mind that a pipeline is only triggered, when it matches the rules defined on the setup page for your repository.