# Cancel or restart jobs
- Copy the link of the ``Details`` link next to the CSCS CI status of your project and remove the
  ending ``type=gitlab``.
- Or paste directly the link with corresponding values for your project: [https://cicd-ext-mw.cscs.ch/ci/pipeline/results/<pipeline_id>/<project_id>/<pipeline_nb>](https://cicd-ext-mw.cscs.ch/ci/pipeline/results/<pipeline_id>/<project_id>/<pipeline_nb>) (the IDs can be found on the Gitlab page of your mirror project).
- Click on ``Login to restart jobs`` at the bottom of this page.
- Enter the same login as the one for your project on the [CI setup page](https://cicd-ext-mw.cscs.ch/ci/setup/ui).
- Click ``Cancel running`` or ``Restart jobs``.
