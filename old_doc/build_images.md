# Building images via API
It is possible to build a container image only via an API call, without registering a git repository. It should be noted, that this is discouraged, because it makes it will make your life worse, since your changes are not tracked in a sourcecode repository. But for a quick and dirty test this is a good starting point.

To use the API endpoint you need to create a API credentials (one time setup). For this you need to head to the [developer portal](https://developer.cscs.ch/devportal/applications).
- Add a new application (choose a name you like, e.g. `container-image-builder`)
- Go to the `OAuth2 Tokens` tab, and click at the bottom on `Generate tokens`. Note down the `Consumer Key` and `Consumer secret`.
- Go to the `Subscriptions` tab and subscribe to `ciext-container-builder`.

Now your application is created, in the developer portal at the top there is the tab `APIs`, which allows you to inspect the API for `ciext-container-builder`. Very helpful there is the section `Documents` with the endpoint documentation for `/container/build POST`.

All parameters are documented there, so please refer to the documentation there. Basically the endpoint can work in two different modes:
1. Send just a Dockerfile
2. Send a full build context as a tar.gz-archive and tell the API where the Dockerfile inside the tarball is.

## Create access token
To send any request to the API endpoint, we first need to create an access token, which can be done with the `Consumer Key` and `Consumer secret`.
```
ACCESS_TOKEN="$(curl  -u <your-consumer-secret>:<your-consumer-secret> --silent -X POST https://auth.cscs.ch/auth/realms/firecrest-clients/protocol/openid-connect/token -d "grant_type=client_credentials" | jq -r '.access_token')"
```
It is also ok, to not specify the consumer secret on the commanline and only send the consumer key, then you will be prompted for the consumer secret when you run the command. The token is stored in the variabe `ACCESS_TOKEN`. This token has only a short validity, so you need to create a fresh access token, whenever the current one becomes invalid (about 5 minutes validity).


## 1. mode - Send a Dockerfile
Now we can call the API endpoint:
```
curl -H "Authorization: Bearer $ACCESS_TOKEN" --data-binary @path/to/Dockerfile "https://api.cscs.ch/ciext/v1/container/build?arch=x86_64"
```

It is mandatory to specify for which architecture you want to build the container. Valid choices are:
- `x86_64` - Intel/AMD architecture - Correct for all nodes that are *not* Grace-Hopper at CSCS
- `aarch64` - Arm architecture - Correct for Grace-Hopper

The API call above sends the Dockerfile to the server, and the server will reply with a link, where you can see the build log. The final container image will be pushed to JFrog, a CSCS internal container registry. On any CSCS machine you can then pull the container image, once it is finished building.

If you want to push the image to your dockerhub account, you need to create a dockerhub access token with write permissions, and then use th API call (similarly for other OCI registry providers)
```
curl -H "Authorization: Bearer $ACCESS_TOKEN" -H "X-Registry-Username <your-dockerhub-username>" -H "X-Registry-Password: <your-dockerhub-token>" --data-binary @path/to/Dockerfile "https://api.cscs.ch/ciext/v1/container/build?arch=x86_64&image=docker.io/<your-dockerhub-username>/my_image_name:latest"

```

## 2. mode - Full tar.gz-archive as build context
Lastly we want to present the second mode, where you send a full tar.gz archive to the server. This is needed in cases where you want to use inside your dockerfile the `COPY` or `ADD` statement. To send a full archive the easiest is via the API call
```
cd path/to/build_context
tar -czf - . | curl -H "Authorization: Bearer $ACCESS_TOKEN" --data-binary @- "https://api.cscs.ch/ciext/v1/container/build?arch=x86_64&dockerfile=relative/path/to/Dockerfile"

```
This is similar to 
```
docker build -f relative/path/to/Dockerfile .
```
Of course you can also specify a custom image, see above example for a custom image name.