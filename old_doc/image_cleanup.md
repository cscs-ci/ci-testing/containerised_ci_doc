## Automatic image cleanup
There is an automatic image cleanup happening in the background according to the following rules:
- No deletion if used storage < 300GB
- No deletion of images newer than 30 days
- First cleanup excluding folders `base`, `baseimg`, `baseimage`, `deploy`, `deployment`
  - Delete images that have been unused for more than 30 days (usage means download)
- Second cleanup if storage usage is still > 300GB
  - Delete images in the above mentioned excluded folders if image has been unused for more than 365 days